"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var PIXI = _interopRequireWildcard(require("pixi.js"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var Application = PIXI.Application;
var Container = PIXI.Container;
var _PIXI$Loader = PIXI.Loader,
    loader = _PIXI$Loader.loader,
    resources = _PIXI$Loader.resources;
var Graphics = PIXI.Graphics;
var TextureCache = PIXI.utils.TextureCache;
var Sprite = PIXI.Sprite;
var Text = PIXI.Text;
var TextStyle = PIXI.TextStyle;
var app = new Application({
  width: 512,
  height: 512,
  antialias: true
});
document.body.appendChild(app.view);
var state;
var scoreBar;
var value = 0;
var score;
var target;
var gameScene;
var id;
var bg;
var timer = 10;
var targetClick = true;
app.loader.add('../images/sprites/atlas.json').load(setup);

function setup() {
  console.log(resources);
  id = resources['../images/sprites/atlas.json'].textures;
  gameScene = new Container();
  app.stage.addChild(gameScene);
  bg = new Sprite(id['bg.png']);
  bg.anchor.set(0, 0);
  gameScene.addChild(bg);
  var scoreBar = new Container();
  scoreBar.position.set(app.stage.width / 2 - scoreBar.width / 2, 22);
  gameScene.addChild(scoreBar);
  var bgScoreBar = new Sprite(id['score.png']);
  scoreBar.addChild(bgScoreBar);
  var style = new TextStyle({
    fontFamily: 'Arial',
    fontSize: 28,
    fill: 'while'
  });
  score = new Text('0', style);
  score.x = -score.width / 2;
  score.y = -score.height / 2 - 1;
  scoreBar.addChild(score);
  target = new Sprite(id['cookie.png']);
  target.x = gameScene.width / 2;
  target.y = gameScene.height / 2;
  target.interactive = true;
  target.buttonMode = true;
  target.on('pointerdown', handlerClick);
  gameScene.addChild(target);
  state = play;
  app.ticker.add(function (delta) {
    return gameLoop(delta);
  });
}

function gameLoop(delta) {
  state(delta);
}

function play() {
  if (timer === 0) {
    targetClick = true;
    target.scale.x = 1;
    target.scale.y = 1;
  } else if (timer > 0) {
    timer--;
  }
}

function handlerClick() {
  if (targetClick) {
    value++;
    score.text = value;
    score.x = -score.width / 2;
    score.y = -score.height / 2;
    target.scale.x = 0.95;
    target.scale.y = 0.95;
    targetClick = false;
    timer = 10;
  }
}