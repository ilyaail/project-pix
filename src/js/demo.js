import * as PIXI from 'pixi.js';

const { Application } = PIXI;
const { Container } = PIXI;
// const { loader } = PIXI.Loader.shared;
const { resources } = PIXI.Loader.shared;
const { Graphics } = PIXI;
const { TextureCache } = PIXI.utils;
const { Sprite } = PIXI;
const { Text } = PIXI;
const { TextStyle } = PIXI;

const app = new Application({
  width: 900,
  height: 800,
  antialias: true,
});

document.body.appendChild(app.view);

let state;
let scoreBar;
let value = 0;
let score;
let target;
let gameScene;
let id;
let bg;
let timer = 1;
let targetClick = true;
let targetBible;
let bibleCost = 10;
let bibleCount;

PIXI.Loader.shared.add('../images/sprites/atlas.json').load(setup);

function setup() {
  console.log(resources);
  id = resources['../images/sprites/atlas.json'].textures;

  gameScene = new Container();
  app.stage.addChild(gameScene);

  bg = new Sprite(id['bg.png']);
  bg.anchor.set(0, 0);
  gameScene.addChild(bg);

  const scoreBar = new Container();
  scoreBar.position.set(app.stage.width / 2 - scoreBar.width / 2, 22);
  gameScene.addChild(scoreBar);

  const bgScoreBar = new Sprite(id['scorebar.png']);
  scoreBar.addChild(bgScoreBar);

  const style = new TextStyle({
    fontFamily: 'Arial',
    fontSize: 28,
    fill: 'white',
  });
  const bibleText = new TextStyle({
    fontFamily: 'Arial',
    fontSize: 18,
    fill: 'white',
  });
  score = new Text('0', style);
  score.x = -score.width / 2;
  score.y = -score.height / 2 - 1;
  scoreBar.addChild(score);

  bibleCount = new Text('0', bibleText);

  target = new Sprite(id['sprite-zinya.png']);
  target.x = gameScene.width / 2;
  target.y = gameScene.height / 2;
  target.interactive = true;
  target.buttonMode = true;
  target.on('pointerdown', handlerClick);

  //  click hendler on bible
  targetBible = new Sprite(id['bible.png']);
  targetBible.x = gameScene.width / 2;
  targetBible.y = gameScene.height - 100;
  targetBible.interactive = true;
  targetBible.buttonMode = true;
  targetBible.on('pointerdown', bibleClick);
  bibleCount.x = -targetBible.width / 2;
  bibleCount.y = -targetBible.height / 2;
  gameScene.addChild(target);

  state = play;
  app.ticker.add((delta) => gameLoop(delta));
}

function gameLoop(delta) {
  state(delta);
}

function play() {
  if (timer === 0) {
    targetClick = true;

    target.scale.x = 1;
    target.scale.y = 1;
  } else if (timer > 0) {
    timer--;
  }
}

function handlerClick() {
  if (targetClick) {
    value++;
    score.text = value;
    if (value >= 10) {
      gameScene.addChild(targetBible);
    }
    score.x = -score.width / 2;
    score.y = -score.height / 2;

    target.scale.x = 0.95;
    target.scale.y = 0.95;

    targetClick = false;

    timer = 1;
  }
}

function bibleClick() {
  if (targetBible) {
    value -= bibleCost;
    score.text = value;
    score.x = -score.width / 2;
    score.y = -score.height / 2;
    bibleCount.text++;
    targetBible.addChild(bibleCount);
    bibleCount.x = -targetBible.width + 80;
    bibleCount.y = -targetBible.height + 120;
    window.setInterval(function () {
      value = value + 1;
      score.text = value;
    }, 1000);
  }
  